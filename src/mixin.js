import { apiPostBet2 } from '@/api.js'
import moment from 'moment'
export const globalMixin = {
    data() {
        return {
            ocs:{
                lot_types: {1:'时时彩'
                            , 2:'十一选五'
                            , 3:'排列三'
                            , 4:'快乐十分'
                            , 6:'PK拾'
                            , 8:'快乐8'
                            , 9:'快三'
                            , 11:'PC蛋蛋'
                            , 12:'六合彩'},
                zodiac: {'鼠': [1,13,25,37,49]
                        ,'牛': [12,24,36,48]
                        ,'虎': [11,23,35,47]
                        ,'兔': [10,22,34,46]
                        ,'龙': [9,21,33,45]
                        ,'蛇': [8,20,32,44]
                        ,'马': [7,19,31,43]
                        ,'羊': [6,18,30,42]
                        ,'猴': [5,17,29,41]
                        ,'鸡': [4,16,28,40]
                        ,'狗': [3,15,27,39]
                        ,'猪': [2,14,26,38]},
                pcdd_color:{
                            '红':[3,6,9,12,15,18,21,24],
                            '绿':[1,4,7,10,16,19,22,25],
                            '蓝':[2,5,8,11,17,20,23,26],
                        },
                lhc_color:{
                            '红':[1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46],
                            '蓝':[3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48],
                            '绿':[5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49],
                        },
                zh_subname:{

                },
                generateType:function(type_id)
                {
                    var type = 0;
                    if([1,5].indexOf(type_id)!=-1)
                    {
                        type = 1;//时时彩
                    }
                    if([6].indexOf(type_id)!=-1)
                    {
                        type = 2;//十一选五
                    }
                    if([9,10,11].indexOf(type_id)!=-1)
                    {
                        type = 3;//排列三
                    }
                    if([17].indexOf(type_id)!=-1)
                    {
                        type = 4;//快乐十分
                    }
                    if([20,61,75].indexOf(type_id)!=-1)
                    {
                        type = 6;//PK拾
                    }
                    if([24,68].indexOf(type_id)!=-1)
                    {
                        type = 8;//快乐8
                    }
                    if([25,60,94].indexOf(type_id)!=-1)
                    {
                        type = 9;//快三
                    }
                    if([65,68].indexOf(type_id)!=-1)
                    {
                        type = 11;//PC蛋蛋
                    }
                    if([90].indexOf(type_id)!=-1)
                    {
                        type = 12;//六合彩
                    }
                    return type;
                },
                generateById: function(type_id, oc){
                        var type = 0;
                        type=this.generateType(type_id);
                        /*
                        if($.inArray(type_id, [1, 8, 17]) > -1){	// 时时彩
                            type = 1;
                        }else if($.inArray(type_id, [2]) > -1){	// 十一选五
                            type = 2;
                        }else if($.inArray(type_id, [3, 10, 13, 14, 15]) > -1){	// 快三
                            type = 3;
                        }else if($.inArray(type_id, [4, 9, 11, 16]) > -1){	// PK拾
                            type = 4;
                        }else if($.inArray(type_id, [5]) > -1){	// PC蛋蛋
                            type = 5;
                        }else if($.inArray(type_id, [6, 12, 18]) > -1){	// 快乐十分
                            type = 6;
                        }else if($.inArray(type_id, [7]) > -1){	// 六合彩
                            type = 7;
                        }else if($.inArray(type_id, [19, 20]) > -1){	// 3D, P3
                            type = 8;
                        } */
                        //console.log(this.zodiac,this.zodiac);
                        return this.generate(type, oc);
                    },
                generate: function(type, oc){
                    var tmp;
                    tmp = oc.split('+');
                    var sp = '';
                    if(tmp.length > 1){
                        sp = tmp[1];
                    }
                    var codes = tmp[0].split(',');

                    var data = {};
                    if(type == 1){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]) + parseInt(codes[3]) + parseInt(codes[4]);
                        data['total'] = total;
                        var totalDx = total >= 23 ? "大": "小";
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
                        var lh = "";
                        if (parseInt(codes[0]) > parseInt(codes[4])) {
                            lh = "龙";
                        } else if (parseInt(codes[0]) == parseInt(codes[4])) {
                            lh = "和";
                        } else {
                            lh = "虎";
                        }
                        data['lh'] = lh;
                    }else if(type == 2){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]) + parseInt(codes[3]) + parseInt(codes[4]);
                        data['total'] = total;
                        var totalDx = "小";
                        if(total == 30){
                            totalDx = "和";
                        }else  if (total > 30) {
                            totalDx = "大";
                        }
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
                        var totalWs = total % 10 >= 5 ? "尾大": "尾小";
                        data['totalWs'] = totalWs;
                        var lh = "";
                        if (parseInt(codes[0]) > parseInt(codes[4])) {
                            lh = "龙";
                        } else {
                            lh = "虎";
                        }
                        data['lh'] = lh;
                    }else if(type == 9 || type == 11){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]);
                        data['total'] = total;
                        var totalDx = "";
                        if(type == 9){
                            if (total >= 11) {
                                totalDx = "大";
                            } else {
                                totalDx = "小";
                            }
                        }else{
                            if (total >= 14) {
                                totalDx = "大";
                            } else {
                                totalDx = "小";
                            }
                        }
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
                        if(type==11)
                        {
                            for(var key2 in this.pcdd_color){
                                if(this.pcdd_color[key2].indexOf(total)!=-1)
                                {
                                    data['color'] = key2;
                                    break;
                                }
                            }
                        }
                    }else if(type == 6){
                        var gyh = parseInt(codes[0]) + parseInt(codes[1]);	// 冠亚和
                        data['gyh'] = gyh;
                        var gydx = gyh == 11?"和":gyh > 11?"大":"小";	// 冠亚大小
                        data['gydx'] = gydx;
                        var gyds = gyh % 2 == 0 ? "双": "单";	// 冠亚单双
                        data['gyds'] = gyds;
                        var lh1 = parseInt(codes[0]) > parseInt(codes[9]) ? "龙": "虎";
                        data['lh1'] = lh1;
                        var lh2 = parseInt(codes[1]) > parseInt(codes[8]) ? "龙": "虎";
                        data['lh2'] = lh2;
                        var lh3 = parseInt(codes[2]) > parseInt(codes[7]) ? "龙": "虎";
                        data['lh3'] = lh3;
                        var lh4 = parseInt(codes[3]) > parseInt(codes[6]) ? "龙": "虎";
                        data['lh4'] = lh4;
                        var lh5 = parseInt(codes[4]) > parseInt(codes[5]) ? "龙": "虎";
                        data['lh5'] = lh5;
                    }else if(type == 4){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]) + parseInt(codes[3]) + parseInt(codes[4]) + parseInt(codes[5]) + parseInt(codes[6]) + parseInt(codes[7]);
                        data['total'] = total;
                        var totalDx = "";
                        if (total > 84) {
                            totalDx = "大";
                        } else if (total == 84) {
                            totalDx = "和";
                        } else if (total < 84) {
                            totalDx = "小";
                        }
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
                        var wsDesc = "";
                        if (total % 10 < 5) {
                            wsDesc = "尾小";
                        } else {
                            wsDesc = "尾大";
                        }
                        data['wsDesc'] = wsDesc;
                    }else if(type == 12){
                            for(var key in codes){
                                var val = parseInt(codes[key]);
                                for(var key2 in this.zodiac){
                                    if(this.zodiac[key2].indexOf(val)!=-1){
                                        data['z_' + val] = key2;
                                        break;
                                    }
                                }
                            };
                        // data['split'] = '+';
                        // var val = parseInt(sp);
                        // for(var key2 in this.zodiac){
                        //     if(this.zodiac[key2].indexOf(val)!=-1){
                        //     //if($.inArray(val, ocs.zodiac[key2]) != -1){
                        //         data['z_' + val] = key2;
                        //         break;
                        //     }
                        // }
                    }else if(type == 3){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]);
                        data['total'] = total;
                        var totalDx = total >= 14 ? "大": "小";
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
                        var lh = "";
                        if (parseInt(codes[0]) > parseInt(codes[4])) {
                            lh = "龙";
                        } else if (parseInt(codes[0]) == parseInt(codes[4])) {
                            lh = "和";
                        } else {
                            lh = "虎";
                        }
                        data['lh'] = lh;
                    }
                    return data;
                }
            },
        };
    },
    methods: {
        go2page(url){
			this.$router.push({path: url, query: {}});
        },
        reload_page()
        {
            this.$router.go(0);
        },
        str_split(str)
        {
            str = str.toString();
            return str.split(",");
        },
        str_split_4d(str)
        {
            str = str.toString();
            let code_array = str.split(" ");
            let code_return = [];
            for(var n in code_array)
            {
                code_return = code_return.concat(code_array[n].split(","));
                code_return = code_return.concat([' ']);
            }
            return code_return;
        },
        numberPk10(value)
        {
            return parseInt(value);
        },
        checkPosNum(num)
        {
            if (num < 0)
            {
                return false;
            }else
            {
                return true;
            }
        },
        error_func(errID){
            switch(errID)
            {
                case 1:
                    this.$store.dispatch('clearUserInfo');
                    return;
                case 3:
                    //var reload_issue=setInterval(this.reload_page(),3000);
                    //clearInterval(reload_issue);
                    //setTimeout(this.reload_page(),30000);

                    return;
            }
        },
        error_handler(e)
        {
            if(e.func==2)
            {
                this.toast(e.error,null,0);
                return;
            }
            if(e.func==3)
            {
                this.toast(e.error,{func:this.reload_page},3000);
                return;
            }
            this.toast(e.error);
            if(e.func)
            {
                this.error_func(e.func);
            }
            return;
        },
        dialog(type,title,content)
        {
            this.$createDialog({
                type: type,
                title: title,
                content: content,
                icon: 'cubeic-alert'
              }).show()
        },
        toast(msg, events,time=2000,type='txt'){
                if(events!=null)
                {
                    events={
                        timeout: events.func
                    };
                }
                this.$createToast({
                    txt: msg,
                    type: type,
                    time: time,
                    mask: true,
                    $events: events
                }).show();
        },
        back() {
            this.$router.back()
        },
        addBet_zh(e) {
            var str = e.target.className;
            var target = e.target;
            if (str == "" || str == "game-title" || str.indexOf("icon") > -1 || str.indexOf("cubeic") > -1) { return; }
            if (str.indexOf("balls") > -1 || str.indexOf("square") > -1 || str.indexOf("pl") > -1 || str.indexOf("k3") > -1 || str.indexOf("zh_img") > -1) {
                target = e.target.parentElement;
                str = target.className;
            }
            var playedId = target.dataset.id;
            if (str.indexOf("name") > -1 && str.indexOf("addbet_zh") > -1) {
                target.className = str.replace(" addbet_zh", "");
            }
            if (str.indexOf("name") > -1 && str.indexOf("addbet_zh") == -1) {
                target.className = str + " addbet_zh";
            }

            if (this.betData.length > 0) {
                if (this.delBet(playedId)) {
                    return;
                }
            }
            var Data = {
                playedId: playedId,
            };
            this.$store.commit('AddbetData', Data);
            this.$store.commit('AddbetFlag', { "playedId": playedId, "target": target });
            return;
        },
        addBet(e)
        {
            var str=e.target.className;
            var target=e.target;
            if(str=="" || str=="game-title" || str.indexOf("icon")>-1 || str.indexOf("cubeic")>-1){return;}
            if (str.indexOf("balls") > -1 || str.indexOf("square") > -1 || str.indexOf("pl") > -1 || str.indexOf("k3") > -1 || str.indexOf("zh_img") > -1)
            {
                target=e.target.parentElement;
                str=target.className;
            }
            var playedId=target.dataset.id;
            if(!playedId)
            {
                return;
            }
            if(str.indexOf("name")>-1 && str.indexOf("addbet")>-1)
            {
                target.className=str.replace(" addbet", "");
            }
            if(str.indexOf("name")>-1 && str.indexOf("addbet")==-1)
            {
                target.className=str+" addbet";
            }

            if(this.betData.length>0)
            {
                if(this.delBet(playedId))
                {
                    return;
                }
            }
            var Data={
                playedId:playedId,
            };
            this.$store.commit('AddbetData',Data);
            this.$store.commit('AddbetFlag',{"playedId":playedId,"target":target});
            return;
        },
        delBet(playedId)
        {
            var check =false;
            for (var i= 0; i< this.betData.length; i++){
                if(this.betData[i].playedId==playedId)
                {
                    this.$store.commit('DelbetData',i);
                    check = true;
                    break;
                }
            }
            for (var i= 0; i< this.betFlag.length; i++){
                if(this.betFlag[i].playedId==playedId)
                {
                    this.$store.commit('DelbetFlag',i);
                    check = true;
                    break;
                }
            }
            if(check==true)
            {
                return true;
            }
        },
        restBet()
        {
            if(this.betFlag.length > 0)
            {
                for (var i= 0; i< this.betFlag.length; i++){
                    var target = this.betFlag[i].target;
                    var str=target.className;
                    str=str.replace(" addbet", "");
                    str = str.replace(" addbet_zh", "");
                    target.className=str.replace(" addOF", "");
                }
            }
            this.$store.commit('RestBet');
        },
        postBet()
        {
            var code=[];
            if(this.betData.length <=0)
            {
                this.toast("请先选择号码");
                return;
            }
            if(this.coin <=0)
            {
                this.toast("请输入金额");
                return;
            }
            if(this.currentIssue ==null)
            {
                this.toast("尚未获取奖期");
                return;
            }
            if(this.user.username =='')
            {
                this.toast("请先登录");
                return;
            }
            let betAmount = 0;
            for (var i= 0; i< this.betData.length; i++){
                var playedId=this.betData[i].playedId;
                if (parseInt(this.coin) < this.config.min_bet) {
                    this.toast("单笔投注金额不得小于 " + this.config.min_bet);
                    return;
                }
                betAmount += parseInt(this.coin);
                code.push({
                    fanDian:0,
                    bonusProp:this.mapByPlay[playedId].bp,
                    actionData:this.mapByPlay[playedId].name,
                    playedId:playedId,
                    actionAmount:this.coin,
                    type:this.type_id,
                    //orderId:this.$faker().random.uuid(),
                });
            }
			if(betAmount > this.user.coin)
			{
                this.toast("钱包余额不足,请先充值");
                return;
			}
            this.toast('Loading...',null,0,'loading');
            apiPostBet2({
                code : code,
                para:{
                    type:this.type_id,
                    actionNo:this.currentIssue.actionNo,
                    kjTime:this.currentIssue.actionTime,
                    betType:1
                },
            })
            .then(response => {
                this.$createToast().hide();
                if(response.data.error)
                {
                    this.error_handler(response.data);
                }
                if(response.data.status)
                {
                    this.$store.dispatch('setCoin',response.data.coin);
                    this.toast(response.data.msg);
                    this.showme = false;
                }
                return;
            }).catch(error => {
                console.log(error);
                return;
            });
        },
        getStatus(item){
            if(item.isDelete==1)
            {
                return "已撤单";
            }
            if(item.lotteryNo!="")
            {
                return "已开奖";
            }
            if(item.lotteryNo=="")
            {
                return "未开奖";
            }
        },
        getCashStatus(item){
            switch(item.state)
            {
                case 1:
                    return "申请中";
                case 2:
                    return "已取消";
                case 3:
                    return "已出款";
                case 4:
                    return "提款失败";
                case 0:
                    return "已出款";
            }
        },
        getRechargeStatus(item){
            switch(item.state)
            {
                case 0:
                    return "申请中";
                case 1:
                    return "成功到账";
                case 2:
                    return "存款失败";
            }
        },
        getClientType(type){
            switch(type)
            {
                case 0:
                    return "PC端";
                case 1:
                    return "手机端";
            }
        },
        getMomentFullDate(time)
        {
			if(time=="")
			{
				return "--";
			}
            time=time*1000;
            return moment(time).format("YYYY-MM-DD HH:mm");
        },
        getMomentYearDate(time)
        {
            time=time*1000;
            return moment(time).format("YYYY-MM-DD");
        },
        getMomentDate(time)
        {
            time=time*1000;
            return moment(time).format("MM-DD");
        },
        getMomentTime(time)
        {
            time=time*1000;
            return moment(time).format("HH:mm");
        },
        pcdd_code(str)
        {
            var codes = str.split(',');
            var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]);
            for(var key2 in this.pcdd_color){
                if(this.pcdd_color[key2].indexOf(total)!=-1)
                {
                    data['color'] = key2;
                    break;
                }
            }
            return total;
        },
        pcdd_color(str)
        {
            var codes = str.split(',');
            var total=0;
            if(codes.length==1)
            {
                total=parseInt(codes[0]);
            }else
            {
                total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]);
            }
            for(var key2 in this.ocs.pcdd_color){
                if(this.ocs.pcdd_color[key2].indexOf(total)!=-1)
                {
                    if(key2=="红")
                    {
                        return "pcdd_red";
                    }
                    if(key2=="绿")
                    {
                        return "pcdd_green";
                    }
                    if(key2=="蓝")
                    {
                        return "pcdd_blue";
                    }
                }
            }
            return "pcdd_no";
        },
        lhc_color(str)
        {
            var total=parseInt(str);
            for(var key2 in this.ocs.lhc_color){
                if(this.ocs.lhc_color[key2].indexOf(total)!=-1)
                {
                    if(key2=="红")
                    {
                        return "lhc_bg_red";
                    }
                    if(key2=="绿")
                    {
                        return "lhc_bg_green";
                    }
                    if(key2=="蓝")
                    {
                        return "lhc_bg_blue";
                    }
                }
            }
        },
		gethms(time)
		{
			let msec = 0;
			let day = 0;
			let hr = 0;
			let min = 0;
			let sec = 0;
			msec = time;
			day = parseInt(msec / 60 / 60 / 24);
			hr = parseInt(msec  / 60 / 60 % 24);
			min = parseInt(msec / 60 % 60);
			sec = parseInt(msec % 60);            
			return {
                day:day > 9 ? day : '0' + day,
				hr:hr > 9 ? hr : '0' + hr,
				min:min > 9 ? min : '0' + min,
				sec:sec > 9 ? sec : '0' + sec
			}
		}
    },

}

export default {
    AddbetData(state,Data)
    {
        //this.state.betData.push(Data);
        this.state.betData = this.state.betData.concat(Data);
    },
    DelbetData(state,index)
    {
        this.state.betData.splice(index,1);
    },
    DelbetDataAll(state)
    {
        this.state.betData.splice(0,this.state.betData.length);
    },
    AddbetFlag(state,Data)
    {
        this.state.betFlag = this.state.betFlag.concat(Data);
    },
    DelbetFlag(state,index)
    {
        this.state.betFlag.splice(index,1);
    },
    DelbetFlagAll(state)
    {
        this.state.betFlag.splice(0,this.state.betFlag.length);
    },
    // AddbetFlagOF(state,Data)
    // {
    //     this.state.betFlagOF.push(Data);
    // },
    // DelbetFlagOF(state,index)
    // {
    //     this.state.betFlagOF.splice(index,1);
    // },
    SetSwitchPlay(state,play)
    {
        this.state.switchPlay=play;
    },
    SetMoveTips(state,status)
    {
        this.state.MoveTipsRead=status;
    },
    SetIssue(state,issue)
    {
        this.state.currentIssue=issue;
    },
    SetBeiShu(state,num)
    {
        this.state.beiShu=num;
    },
    SetLastIssue(state,issue)
    {
        this.state.lastIssue=issue;
    },
    SetRaceCountdown(state,countdown)
    {
        this.state.raceCountdown=countdown;
    },
    SetCoinScore(state,info)
    {
        this.state.user.coin=info.coin;
        this.state.user.scoreTotal=info.scoreTotal;
        this.state.user.grade=info.grade;
    },
    RestBet()
    {
        this.state.betData.splice(0,this.state.betData.length);
        this.state.betFlag.splice(0,this.state.betFlag.length);
    }
}

import axios from 'axios'
import { apiGetGroup,apiGetCoin,apiGetType,apiGetBp} from "../api.js";
export default {
    // getUserInfo({ commit }) {
    //     apiGetUserData()
    //         .then(response => {
    //             this.state.user.isLogin = response.data.status;
    //             let data = response.data.data;
    //             this.state.user.uid = data.uid;
    //             this.state.user.name = data.name;
    //         })
    //         .catch(error => {
    //             console.log(error);
    //         });
    // },
    setbetCountdown({ commit },time) {
        this.state.betCountdown.hr = time.hr;
        this.state.betCountdown.min = time.min;
        this.state.betCountdown.sec = time.sec;
    },
    setUserInfo({ commit },user) {
        this.state.user = user;
    },
    clearUserInfo({ commit }) {
        this.state.user = {
            uid : 0,
            username : '',
            coin:0,
            type : 0,
            scoreTotal:0,
            grade:0,
            fanDian:0,            
        };
    },
    getCoin({ commit ,dispatch})
    {
        clearInterval(this.state.coin_time_line);
        apiGetCoin({
            })
            .then(response => {
                if(response.data.error)
                {
                    //var f=error_func(response.data.func);
                    //f.func();
                    return;
                }
                //commit('SetCoinScore',response.data.data);
                dispatch('setUserInfo',response.data.data);
                this.state.coin_time_line= setInterval(() => {
                    dispatch('getCoin');
                }, 15000);
            })
            .catch(error => {
                console.log(error);
            });  
    },
    // setCfg({ commit ,dispatch},type_id)
    // {
    //     apiGetCfg({
    //         cfg_key : null,
    //         type_id : type_id,
    //     })
    //     .then(response => {
    //         this.state.betCfg = response.data;
    //         //dispatch('setmapPlay',this.state.betCfg.groups);
    //         dispatch('setmapPlay',this.state.betCfg.played);
    //     }).catch(error => {
    //         console.log(error);
    //     });
    // },
    setType({commit})
    {
        apiGetType({
        })
        .then(response => {
            this.state.mapByType = response.data.lottery;
            this.state.mapByEgame = response.data.egame;
            var kfurl=response.data.config.kfUrl;
            if(kfurl)
            {
                this.state.config.kfUrl = kfurl.replace(new RegExp("amp;","g"), "");
            }
            this.state.config.dzp_score=response.data.config.dzp_score;
            this.state.config.app_name=response.data.config.app_name;
            this.state.config.domain_name=response.data.config.domain_name;
            this.state.config.platform_type=response.data.config.platform_type;
            document.title = response.data.config.app_name;
        }).catch(error => {
            console.log(error);
        });
    },
    setCoin({commit},coin)
    {
        this.state.user.coin=coin;
    },
    setScore({commit})
    {
        this.state.user.score-=this.state.config.dzp_score;
    },
    setBp({commit ,dispatch},type_id)
    {
        apiGetBp({
            type_id:type_id
        })
        .then(response => {
            this.state.betCfg[response.data.group_type]=true;
            this.state.mapByPlay=Object.assign(this.state.mapByPlay, response.data.played);
            for( var key in response.data.played)
            {
                if(!this.state.mapByGroupToPlay[response.data.played[key].playedGroup])
                {
                    this.state.mapByGroupToPlay[response.data.played[key].playedGroup] = [];
                }
                this.state.mapByGroupToPlay[response.data.played[key].playedGroup].push(response.data.played[key]);
            }
        }).catch(error => {
            console.log(error);
        });
    },
    setGroup({commit ,dispatch},type_id)
    {
        apiGetGroup({
            type_id:type_id
        })
        .then(response => {
            this.state.betGroupCheck[response.data.group_type]=true;
            this.state.mapByGroup=Object.assign(this.state.mapByGroup, response.data.groups);
        }).catch(error => {
            console.log(error);
        });
    },
//     setmapPlay({ commit,state },playeds)
//     {
//         //console.log(groups);
//  /*        for(var k1 in groups)
//         {
//             //console.log(groups[k]);
//             for(var k2 in groups[k1])
//             {
//                 //console.log(groups[k1][k2].playeds);
//                 //state.mapByPlay[]=
//                 for(var k3 in groups[k1][k2].playeds)
//                 {
//                     //console.log(groups[k1][k2].playeds);
//                     state.mapByPlay[groups[k1][k2].playeds[k3].id]=groups[k1][k2].playeds[k3];
//                     //state.mapByPlay.push(groups[k1][k2].playeds[k3]);
                    
//                 }
//             }
//         } */
//         state.mapByPlay=playeds;
//         //state.mapByPlay.sort();
//         //console.log(state.mapByPlay);
//     },
}

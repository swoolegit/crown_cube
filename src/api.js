import axios from 'axios';
import Qs from 'qs';
import GlobalApp from './store/GlobalApp.js';

const apiRequest = axios.create({
	baseURL:  GlobalApp.config.ApiUrl,
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
	},
	transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
		data = Qs.stringify(data)
		return data
	  }],
});

export { apiRequest }

//export const apiGetCurrentIssue = data => apiRequest.post('/get_current_issue',data);
export const apiGetRecentOpenCode = data => apiRequest.post('/index/get_recent_open_code',data);
//export const apiGetCfg = data => apiRequest.post('/get_cfg2',data);
//export const apiPostBet2 = data => apiRequest.post('/bet2',data); // 信用玩法
//export const apiGetCoin = data => apiRequest.post('/get_coin',data); // 获取余额
//export const apiGetBetLog = data => apiRequest.post('/bet_log',data); // 获取注单

//export const apiGetCfg = data => apiRequest.post('/index/get_cfg',data);
export const apiGetType = data => apiRequest.post('/index/get_type',data);
export const apiGetBp = data => apiRequest.post('/index/get_bp',data); //取得賠率
export const apiGetGroup = data => apiRequest.post('/index/get_group',data);
export const apiGetTodayLast = data => apiRequest.post('/index/get_today_last',data); //取得今天最后一期
export const apiGetCurrentIssue = data => apiRequest.post('/index/get_current_issue',data); //取得當前獎期
export const apiGetBinaryIssue = data => apiRequest.post('/index/get_binary_current',data); //取得2元獎期
export const apiPostReg = data => apiRequest.post('/index/reg',data); //注册
export const apiPostLogin = data => apiRequest.post('/index/login',data); //登录
export const apiPostGuestLogin = data => apiRequest.post('/index/guest_login',data); //登录
export const apiGetPromotion = data => apiRequest.post('/index/promotion',data); //优惠活动
export const apiGetBulletin = data => apiRequest.post('/index/bulletin',data); //系统公告
export const apiGetHotlost = data => apiRequest.post('/index/hotlost',data); //冷热遗漏
export const apiGetClong = data => apiRequest.post('/index/clong',data); //冷热遗漏

export const apiGetEgame = data => apiRequest.post('/egame/start',data); //登录电子游戏
export const apiGetECoin = data => apiRequest.post('/egame/coin',data); // 获取原厂余额
export const apiGetECoinIn = data => apiRequest.post('/egame/transferIn',data); // 获取钱包转入
export const apiGetECoinOut = data => apiRequest.post('/egame/transferOut',data); // 获取钱包转出
export const apiGetLetter = data => apiRequest.post('/user/letter',data); //站内信
export const apiGetCheckLetter = data => apiRequest.post('/user/check_letter',data); //检查未读站内信
export const apiPostLetter = data => apiRequest.post('/user/read_letter',data); //检查未读站内信

export const apiGetCodeLD = data => apiRequest.post('/user/ld_code',data); //玩家老搗開獎
export const apiGetCoinLog = data => apiRequest.post('/user/coin',data); //资金纪录
export const apiGetBetLog = data => apiRequest.post('/user/bet_log',data); // 获取彩票注单
export const apiGetEBetLog = data => apiRequest.post('/user/ebet_log',data); // 获取电子注单
export const apiGetCoin = data => apiRequest.post('/user/get_coin',data); // 获取余额
export const apiGetSafe = data => apiRequest.post('/user/safe',data); // 获取安全状态
export const apiPostSafeUp = data => apiRequest.post('/user/safe_up',data); // 获取安全状态
export const apiGetBank = data => apiRequest.post('/user/bank',data); // 获取银行列表
export const apiPostCash = data => apiRequest.post('/user/cash',data); //申请提款
export const apiGetCashLog = data => apiRequest.post('/user/cash_log',data); //提款查找
export const apiGetRecharge = data => apiRequest.post('/user/recharge',data); // 获取充值消息
export const apiPostRecharge = data => apiRequest.post('/user/recharge_post',data); // 提交充值消息
export const apiPostThirdRecharge = data => apiRequest.post('/user/third_recharge_post',data); // 提交充值消息
export const apiGetRechargeLog = data => apiRequest.post('/user/recharge_log',data); //提款查找
export const apiPostLogout = data => apiRequest.post('/user/logout',data); //登出

export const apiPostBet2 = data => apiRequest.post('/game/bet_credit',data); // 信用玩法投注
export const apiPostBet1 = data => apiRequest.post('/game/bet_official',data); // 官方玩法投注
export const apiPostBetMax4D = data => apiRequest.post('/game/bet_max4d',data); // 越南彩Max4D

export const apiAgentSearch = data => apiRequest.post('/agent/search',data); //代理中心
export const apiAgentSearchSub = data => apiRequest.post('/agent/search_sub', data); //代理中心 查找下级
export const apiAgentAdd = data => apiRequest.post('/agent/add',data); //代理中心 添加会员
export const apiAgentList = data => apiRequest.post('/agent/list',data); //代理中心 会员列表
export const apiAgentUpdate = data => apiRequest.post('/agent/up',data); //代理中心 修改会员返点
export const apiAgentAmountUP = data => apiRequest.post('/agent/amount_up', data); //代理中心 代理上分(占成)
export const apiAgentAmountDOWN = data => apiRequest.post('/agent/amount_down', data); //代理中心 代理下分(占成)
export const apiAgentQR = data => apiRequest.post('/agent/qr',data); //代理中心 取得邀请码
export const apiAgentQRupdate = data => apiRequest.post('/agent/qrup',data); //代理中心 修改邀请码

export const apiActivityDzp = data => apiRequest.post('/activity/rotary_submit',data); //大转盘



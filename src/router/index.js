import VueRouter from 'vue-router'
import routes from './routes'

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
	var regex_user = /^\/user\//i;
	var regex_agent = /^\/agent\//i;
	if (to.path.search(regex_user) === -1 && to.path.search(regex_agent) === -1) {
		next();
	}else
	{
		if(router.app.$options.store.getters.user.username=='' || router.app.$options.store.getters.user.forTest==1)
		{
			return next({path: "/"});
		}
		next();
	}
})

export default router



/*
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
})
*/

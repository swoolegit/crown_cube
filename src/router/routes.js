import DefaultLayout from '@/pages/layout/Default.vue';

const routes = [
{
    path: '/',
    component: DefaultLayout,
    children: [
        {
            path: '', //首页
            component: () => import('../pages/index.vue'),
        },
        {
            path: '/index/reg', //注册
            component: () => import('../pages/index/reg.vue'),
        },
        {
            path: '/index/login', //登录
            component: () => import('../pages/index/login.vue'),
        },
        {
            path: '/user/cash/index', //会员中心 現金版
            component: () => import('../pages/user/cash_index.vue'),
        },
        {
            path: '/user/credit/index', //会员中心 信用版
            component: () => import('../pages/user/credit_index.vue'),
        },
        {
            path: '/user/coin', //会员中心 资金纪录
            component: () => import('../pages/user/coin.vue'),
        },
        {
            path: '/user/safe', //会员中心 安全设置
            component: () => import('../pages/user/safe.vue'),
        },
        {
            path: '/user/withdraw', //会员中心 提款
            component: () => import('../pages/user/withdraw.vue'),
        },
        {
            path: '/user/withdrawLog', //会员中心 提款纪录
            component: () => import('../pages/user/withdraw-log.vue'),
        },
        {
            path: '/user/recharge', //会员中心 充值
            component: () => import('../pages/user/recharge.vue'),
        },
        {
            path: '/user/rechargeLog', //会员中心 充值纪录
            component: () => import('../pages/user/recharge-log.vue'),
        },
        {
            path: '/user/service', //会员中心 服務條款
            component: () => import('../pages/user/service.vue'),
        },
        {
            path: '/user/transfer/:vendor/:game', //会员中心 额度转移
            component: () => import('../pages/user/TransferBox.vue'),
        },
        {
            path: '/index/promotion', //优惠活动
            component: () => import('../pages/index/promotion.vue'),
        },
        {
            path: '/index/egame', //電子遊戲
            component: () => import('../pages/index/egame.vue'),
        },
        // {
        //     path: '/agent/index', //代理中心
        //     component: () => import('../pages/agent/index.vue'),
        // },
        // {
        //     path: '/agent/add', //代理中心 添加下级
        //     component: () => import('../pages/agent/add.vue'),
        // },
        // {
        //     path: '/agent/fhadd', //代理中心 添加下级(占成)
        //     component: () => import('../pages/agent/fh-add.vue'),
        // },
        // {
        //     path: '/agent/list', //代理中心 会员列表
        //     component: () => import('../pages/agent/list.vue'),
        // },
        // {
        //     path: '/agent/fhlist', //代理中心 代理列表(占成)
        //     component: () => import('../pages/agent/fh-list.vue'),
        // },
        // {
        //     path: '/agent/member_list', //代理中心 会员列表(占成)
        //     component: () => import('../pages/agent/member-list.vue'),
        // },
        // {
        //     path: '/agent/qr', //代理中心 邀请码列表
        //     component: () => import('../pages/agent/qr.vue'),
        // },
        // {
        //     path: '/agent/fhqr', //代理中心 邀请码列表
        //     component: () => import('../pages/agent/fh-qr.vue'),
        // },
        {
            path: '/spreg/:id', //代理中心 邀请码注册
            component: () => import('../pages/agent/spreg.vue'),
        },
        {
            path: '/user/mail', //站内消息
            component: () => import('../pages/user/mail.vue'),
        },
        {
            path: '/index/help', //帮助中心
            component: () => import('../pages/index/help.vue'),
        },
        {
            path: '/index/zh_help', //帮助中心 字花
            component: () => import('../pages/index/zh_help.vue'),
        },
    ]
},
{
    path: '/agent/cash',
    component: DefaultLayout,
    children: [
        {
            path: 'index', //代理中心
            component: () => import('../pages/agent/cash/index.vue'),
        },
        {
            path: 'add', //代理中心 添加下级
            component: () => import('../pages/agent/cash/add.vue'),
        },
        {
            path: 'Agentlist', //代理中心 
            component: () => import('../pages/agent/cash/list.vue'),
        },
        {
            path: 'qr', //代理中心 邀请码列表
            component: () => import('../pages/agent/cash/qr.vue'),
        }
    ]
},
{
    path: '/agent/credit',
    component: DefaultLayout,
    children: [
        {
            path: 'index', //代理中心
            component: () => import('../pages/agent/credit/index.vue'),
        },
        {
            path: 'add', //代理中心 添加下级
            component: () => import('../pages/agent/credit/add.vue'),
        },
        {
            path: 'Memberlist',
            //name: '會員列表',
            component: () => import('../pages/agent/credit/member_list.vue'),
        },
        {
            path: 'Agentlist', //代理中心 
            component: () => import('../pages/agent/credit/list.vue'),
        },
        {
            path: 'qr', //代理中心 邀请码列表
            component: () => import('../pages/agent/credit/qr.vue'),
        }
    ]
},
{
    // 信用玩法
    path: '/bet/credit',
    component: DefaultLayout,
    children: [
        {
            path: 'ld', //老捣
            component: () => import('../pages/game/credit/ld.vue'),
        },
        {
            path: 'cqssc', //重庆时时彩
            component: () => import('../pages/game/credit/cqssc.vue'),
        },
        {
            path: 'pk10', //北京赛车
            component: () => import('../pages/game/credit/pk10.vue'),
        },
        {
            path: 'gd11x5', //广东11选5
            component: () => import('../pages/game/credit/gd11x5.vue'),
        },
        {
            path: 'jck3', //极速快3
            component: () => import('../pages/game/credit/jck3.vue'),
        },
        {
            path: 'pcdd', //PC蛋蛋
            component: () => import('../pages/game/credit/pcdd.vue'),
        },
        {
            path: 'gdklsf', //广东快乐10分
            component: () => import('../pages/game/credit/gdklsf.vue'),
        },
        {
            path: 'fc3d', //福彩3D
            component: () => import('../pages/game/credit/fc3d.vue'),
        },
        {
            path: 'gxk3', //广西快3
            component: () => import('../pages/game/credit/gxk3.vue'),
        },
        {
            path: 'jcssc', //极速时时彩
            component: () => import('../pages/game/credit/jcssc.vue'),
        },        
        {
            path: 'jcpk10', //极速赛车
            component: () => import('../pages/game/credit/jcpk10.vue'),
        },
        {
            path: 'jsk3', //江苏骰宝
            component: () => import('../pages/game/credit/jsk3.vue'),
        },
        {
            path: 'xyft', //幸运飞艇
            component: () => import('../pages/game/credit/xyft.vue'),
        },
        {
            path: 'xync', //幸运农场
            component: () => import('../pages/game/credit/xync.vue'),
        },
        {
            path: 'jc3k3', //三分快3
            component: () => import('../pages/game/credit/jc3k3.vue'),
        },
        {
            path: 'pl3', //排列3
            component: () => import('../pages/game/credit/pl3.vue'),
        },
        {
            path: 'lhc', //六合彩
            component: () => import('../pages/game/credit/lhc.vue'),
        },
        {
            path: 'jclhc', //极速六合彩
            component: () => import('../pages/game/credit/jclhc.vue'),
        },
        {
            path: 'jx11x5', //江西11选5
            component: () => import('../pages/game/credit/jx11x5.vue'),
        },
        {
            path: 'jlk3', //吉林快3
            component: () => import('../pages/game/credit/jlk3.vue'),
        },
        {
            path: 'tjssc', //天津时时彩
            component: () => import('../pages/game/credit/tjssc.vue'),
        },
        {
            path: 'jc5ssc', //加拿大五分彩
            component: () => import('../pages/game/credit/jc5ssc.vue'),
        },
        {
            path: 'jc3pk10', //三分赛车
            component: () => import('../pages/game/credit/jc3pk10.vue'),
        },
        {
            path: 'jc5pk10', //五分赛车
            component: () => import('../pages/game/credit/jc5pk10.vue'),
        },
        {
            path: 'jc3lhc', //三分六合彩
            component: () => import('../pages/game/credit/jc3lhc.vue'),
        },
        {
            path: 'tcpk10', //腾讯赛车
            component: () => import('../pages/game/credit/tcpk10.vue'),
        },
        {
            path: 'tcssc', //腾讯分分彩
            component: () => import('../pages/game/credit/tcssc.vue'),
        },
        {
            path: 'jc30sssc', //30秒时时彩
            component: () => import('../pages/game/credit/jc30sssc.vue'),
        },
        {
            path: 'jc30spk10', //30秒赛车
            component: () => import('../pages/game/credit/jc30spk10.vue'),
        },
        {
            path: 'jc30sk3', //30秒快三
            component: () => import('../pages/game/credit/jc30sk3.vue'),
        },
        {
            path: 'zh-at', //字花
            component: () => import('../pages/game/credit/zh_flower.vue'),
        },
        {
            path: 'Max4D', //Max4D
            component: () => import('../pages/game/credit/max4d.vue'),
        },
        {
            path: 'Macaolhc', //澳门六合彩
            component: () => import('../pages/game/credit/Macaolhc.vue'),
        },
        {
            path: 'jcxyft', //极速飞艇
            component: () => import('../pages/game/credit/jcxyft.vue'),
        },
        {
            path: 'jc3xyft', //三分飞艇
            component: () => import('../pages/game/credit/jc3xyft.vue'),
        },
    ]    
},
{
    // 官方玩法
    path: '/bet/official',
    component: DefaultLayout,
    children: [
        {
            path: 'cqssc', //重庆时时彩
            component: () => import('../pages/game/official/cqssc.vue'),
        },
        {
            path: 'gd11x5', //广东11选5
            component: () => import('../pages/game/official/gd11x5.vue'),
        },
        {
            path: 'jx11x5', //江西11选5
            component: () => import('../pages/game/official/jx11x5.vue'),
        },
        {
            path: 'fc3d', //福彩3D
            component: () => import('../pages/game/official/fc3d.vue'),
        },
        {
            path: 'pl3', //排列3
            component: () => import('../pages/game/official/pl3.vue'),
        },
        {
            path: 'jcssc', //极速时时彩
            component: () => import('../pages/game/official/jcssc.vue'),
        },
        {
            path: 'gdklsf', //广东快乐十分
            component: () => import('../pages/game/official/gdklsf.vue'),
        },
        {
            path: 'xync', //重庆快乐十分
            component: () => import('../pages/game/official/xync.vue'),
        },
        {
            path: 'pk10', //北京赛车
            component: () => import('../pages/game/official/pk10.vue'),
        },
        {
            path: 'jsk3', //江苏快三
            component: () => import('../pages/game/official/jsk3.vue'),
        },
        {
            path: 'jlk3', //吉林快三
            component: () => import('../pages/game/official/jlk3.vue'),
        },
        {
            path: 'tjssc', //天津时时彩
            component: () => import('../pages/game/official/tjssc.vue'),
        },
        {
            path: 'gxk3', //广西快三
            component: () => import('../pages/game/official/gxk3.vue'),
        },
        {
            path: 'jck3', //极速快三
            component: () => import('../pages/game/official/jck3.vue'),
        },
        {
            path: 'xyft', //幸运飞艇
            component: () => import('../pages/game/official/xyft.vue'),
        },
        {
            path: 'jcpk10', //极速赛车
            component: () => import('../pages/game/official/jcpk10.vue'),
        },
        {
            path: 'jc3k3', //三分快三
            component: () => import('../pages/game/official/jc3k3.vue'),
        },
        {
            path: 'jc5ssc', //加拿大五分彩
            component: () => import('../pages/game/official/jc5ssc.vue'),
        },
        {
            path: 'jc3pk10', //三分赛车
            component: () => import('../pages/game/official/jc3pk10.vue'),
        },
        {
            path: 'jc5pk10', //五分赛车
            component: () => import('../pages/game/official/jc5pk10.vue'),
        },
        {
            path: 'tcpk10', //腾讯赛车
            component: () => import('../pages/game/official/tcpk10.vue'),
        },
        {
            path: 'tcssc', //腾讯分分彩
            component: () => import('../pages/game/official/tcssc.vue'),
        },
        {
            path: 'jc30sssc', //30秒时时彩
            component: () => import('../pages/game/official/jc30sssc.vue'),
        },
        {
            path: 'jc30spk10', //30秒赛车
            component: () => import('../pages/game/official/jc30spk10.vue'),
        },
        {
            path: 'jc30sk3', //30秒快三
            component: () => import('../pages/game/official/jc30sk3.vue'),
        },
        {
            path: 'jcxyft', //极速飞艇
            component: () => import('../pages/game/official/jcxyft.vue'),
        },
        {
            path: 'jc3xyft', //三分飞艇
            component: () => import('../pages/game/official/jc3xyft.vue'),
        },
    ]    
}
]


export default routes


import router from '@/router'
function findIndex(ary, fn) {
  if (ary.findIndex) {
    return ary.findIndex(fn)
  }
  /* istanbul ignore next */
  let index = -1
  /* istanbul ignore next */
  ary.some(function (item, i, ary) {
    const ret = fn.call(this, item, i, ary)
    if (ret) {
      index = i
      return ret
    }
  })
  /* istanbul ignore next */
  return index
};
function error_func(errID){
  switch(errID)
  {
      case 1:
          return {
              func: () => {
                  router.push("/index/login");
              }                        
          };
  }

};

export {
  findIndex,
  error_func,
}
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import router from './router'
import App from './App'
import Vuex from 'vuex';
Vue.use(Vuex);
import faker from 'vue-faker'
Vue.use(faker);

import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

import Cube from 'cube-ui'
Vue.use(Cube)
Vue.use(VueRouter)

// import waterfall from 'vue-waterfall2'
// Vue.use(waterfall)

import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: './static/img-error.png',
    loading: 'dist/loading.gif',
    attempt: 1
})

import store from './store'

import axios from 'axios';
Vue.prototype.$axios = axios;

Vue.config.productionTip = false


/* router.beforeEach((to, from, next) => {
    
    var regex = /^\/share/i;
    if (to.path.search(regex) === -1) {
        next();
    } else {
        if (!store.getters.user.isLogin) {
            return next({ path: "/login" });
        }
        else {
            next();
        }
    }
    
}) */

Vue.filter("numFilter", function(value) { 
    let tempVal = parseFloat(value).toFixed(3);
    let realVal = tempVal.substring(0, tempVal.length - 1);
    return realVal.replace(/^(-?\d+?)((?:\d{3})+)(?=\.\d+$|$)/, function (all, pre, groupOf3Digital) {
        return pre + groupOf3Digital.replace(/\d{3}/g, ',$&');
      });
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})

/*
new Vue({
    router,
    render(createElement) {
        return createElement(App)
    }
}).$mount('#app')
*/


import './assets/style.css'
